package com.truemeds.truemeds.constants;

public class AppConstant {

    /**
     * get a api key from https://newsapi.org/
     * just register an account and request for an api key
     * when you will get an api key please replace with YOUR_API_KEY
     */
    public static final String BASE_URL = "https://stage-services.truemeds.in/ArticleService/";

}
