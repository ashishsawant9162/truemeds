package com.truemeds.truemeds.retrofit;


import com.truemeds.truemeds.response.ArticleResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiRequest {
    @POST("getArticleListing")
    Call<ArticleResponse> getArticles(

    );
}
