package com.truemeds.truemeds.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Article {
    @SerializedName("description")
    @Expose
    public String description;

    @SerializedName("categoryName")
    @Expose
    public String categoryName;

    @SerializedName("type")
    @Expose
    public int type;

    @SerializedName("author")
    @Expose
    public String author;

    @SerializedName("categoryId")
    @Expose
    public int categoryId;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("url")
    @Expose
    public Object url;

    @SerializedName("createdOn")
    @Expose
    public String createdOn;

    @SerializedName("image")
    @Expose
    public Object image;

    @SerializedName("articleTime")
    @Expose
    public int articleTime;

    @SerializedName("ranking")
    @Expose
    public int ranking;


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Object getUrl() {
        return url;
    }

    public void setUrl(Object url) {
        this.url = url;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public Object getImage() {
        return image;
    }

    public void setImage(Object image) {
        this.image = image;
    }

    public int getArticleTime() {
        return articleTime;
    }

    public void setArticleTime(int articleTime) {
        this.articleTime = articleTime;
    }

    public int getRanking() {
        return ranking;
    }

    public void setRanking(int ranking) {
        this.ranking = ranking;
    }
}
