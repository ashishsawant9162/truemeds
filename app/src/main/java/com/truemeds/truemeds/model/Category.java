package com.truemeds.truemeds.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Category {
    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("id")
    @Expose
    public int id;

}
