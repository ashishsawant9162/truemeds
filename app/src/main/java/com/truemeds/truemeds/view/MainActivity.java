package com.truemeds.truemeds.view;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.truemeds.truemeds.R;
import com.truemeds.truemeds.adapter.ArticleAdapter;
import com.truemeds.truemeds.model.Article;
import com.truemeds.truemeds.view_model.ArticleViewModel;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private RecyclerView article_recycler_view;
    private ProgressBar progress_circular_article;
    private LinearLayoutManager layoutManager;
    private ArticleAdapter adapter;
    private ArrayList<Article> articleArrayList = new ArrayList<>();
    ArticleViewModel articleViewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialization();

        getArticles();
    }

    /**
     * initialization of views and others
     *
     * @param @null
     */
    private void initialization() {
        progress_circular_article = (ProgressBar) findViewById(R.id.progress_circular_article);
        article_recycler_view = (RecyclerView) findViewById(R.id.my_recycler_view);

        // use a linear layout manager
        layoutManager = new LinearLayoutManager(MainActivity.this);
        article_recycler_view.setLayoutManager(layoutManager);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        article_recycler_view.setHasFixedSize(true);

        // adapter
        adapter = new ArticleAdapter(MainActivity.this, articleArrayList);
        article_recycler_view.setAdapter(adapter);

        // View Model
        articleViewModel= new ViewModelProvider(this).get(ArticleViewModel.class);
    }

    /**
     * get  articles from news api
     *
     * @param @null
     */
    private void getArticles() {
        articleViewModel.getArticleResponseLiveData().observe(this, articleResponse -> {
            if (articleResponse != null) {

                progress_circular_article.setVisibility(View.GONE);
                List<Article> articles = articleResponse.getResult().getArticle();
                articleArrayList.addAll(articles);
                adapter.notifyDataSetChanged();
            }
        });
    }
}
