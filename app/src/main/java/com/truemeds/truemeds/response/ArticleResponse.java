package com.truemeds.truemeds.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.truemeds.truemeds.model.Article;
import com.truemeds.truemeds.model.Result;

import java.util.List;

public class ArticleResponse {
    @SerializedName("result")
    @Expose
    private Result result;

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }
}
